import '../../config/node_modules/regenerator-runtime/runtime';
import {scrollCont, rotateCircle, overlayColorchange} from './scripts/locomotive_scroll';
rotateCircle();
overlayColorchange();
import { general, menu } from './scripts/general';
general();
menu();
import {venue} from './scripts/venue';
venue();
import {gallery} from './scripts/gallery';
gallery();
import { swiperInstances } from './scripts/swiper';
swiperInstances();
import { tabs } from './scripts/sliding_tabs';
tabs();
import { form } from './scripts/form';
form();
import { accordionClick } from './scripts/accordion';
accordionClick();
import { highlight } from './scripts/highlight';
highlight();
import { filter } from './scripts/filter';
filter();
import {contentAnimationIn} from './scripts/animationTitle';

contentAnimationIn();
import './scripts/functions';
import './scripts/lazyload';
import './scripts/updatecss';
import './styles/style';

if(parent.document.getElementById('config-bar')) {
    setInterval(function() {
        scrollCont.destroy();
    }, 10)
}