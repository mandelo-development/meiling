var $ = require("../../../config/node_modules/jquery");
async function form() {
    $("body").on("change", ".form-field__select", selectChange);
    $("body").on("click", ".option [type='radio']", selectChange);

    function selectChange(event){
        var type = $(event.currentTarget).attr('type');
        if (type == 'radio'){
            var optionId = $(event.currentTarget).attr('data_id');
        } else {
            var optionId = $(event.currentTarget).find(':selected').data('id');
        }

        $('[data-parent-id]').each(function(e){
            var childItm = $(this);
            var parentId = $($(childItm)[0]).attr('data-parent-id');
            
            if (optionId === parentId){
                $($(childItm)[0]).show();
            } else {
                $($(childItm)[0]).hide();
            }
        });
    }

} export {
    form
}