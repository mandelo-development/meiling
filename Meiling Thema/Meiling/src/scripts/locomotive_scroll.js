import LocomotiveScroll from '../../../config/node_modules/locomotive-scroll';
import gsap from '../../../config/node_modules/gsap';


export const scrollCont = new LocomotiveScroll({
    el: document.querySelector('#js-scroll'),
    smooth: true,
    smoothMobile: true,
    useKeyboard: false,
    lerp: 0.1,
    scrollFromAnywhere: true,
    tablet: {
        smooth: true
    },
    smartphone: {
        smooth: false
    },
});

function overlayColorchange() {
    var overlay = document.querySelector(".header-content-overlay");
    var cta = document.querySelector(".header-cta");
    if (overlay) {
        scrollCont.on('scroll', (instance) => {
            fillOverlay(overlay, instance.scroll.y);
            if (cta) {
                hideCta(cta, instance.scroll.y);
            }
        });

        function fillOverlay(element, pageOffset) {
            var elementHeight = element.clientHeight;
            gsap.to(element, {
                opacity: 1 - (elementHeight - pageOffset) / elementHeight,
            })
        }

        function hideCta(element, pageOffset) {
            var elementHeight = element.clientHeight;
            gsap.to(element, {
                opacity: 1 - (pageOffset - elementHeight) / (elementHeight + 200),
            })
        }
    }
} export {
    overlayColorchange
};

function rotateCircle() {
    var circleSVG = document.querySelector(".rotator");
    if (circleSVG) {
        scrollCont.on('scroll', (instance) => {
            scrollRotate(circleSVG, instance.scroll.y);
        });

        function scrollRotate(element, pageOffset) {
            gsap.to(element, {
                rotate: pageOffset / 10,
            })
        }
    }
} export {
    rotateCircle
};
