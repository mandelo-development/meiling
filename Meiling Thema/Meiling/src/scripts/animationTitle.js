import gsap from '../../../config/node_modules/gsap';
import ScrollTrigger from '../../../config/node_modules/gsap/ScrollTrigger'
var $ = require("../../../config/node_modules/jquery");
var animsition =  require("../../../config/node_modules/animsition/dist/js/animsition");
import {scrollCont} from './locomotive_scroll';
import { SplitText } from '../../../config/node_modules/gsap/SplitText';

gsap.config({nullTargetWarn: false});
gsap.registerPlugin(ScrollTrigger)

var contentTl = gsap.timeline();

async function contentAnimationIn() {
    $(document).ready(function () {
        scrollCont.on('scroll', ScrollTrigger.update);
        ScrollTrigger.scrollerProxy('#js-scroll', {
            scrollTop(value) {
                return arguments.length ? scrollCont.scrollTo(value, 0, 0) : scrollCont.scroll.instance.scroll.y;
            },
            getBoundingClientRect() {
                return {
                    top: 0, left: 0, width: window.innerWidth, height: window.innerHeight
                }
            },
            pinType: document.querySelector('#js-scroll').style.transform ? "transform": "fixed"
        });

        ScrollTrigger.matchMedia({
            "(min-width: 1024px)": function () {
                var titleTimeline = gsap.timeline()

                titleTimeline.from('.images-scroll-text-content-title .animated-title .animated-title__first-part', {
                    scrollTrigger: {
                        trigger: '.animated-title',
                        scroller: '#js-scroll',
                        scrub: 1,
                        start: '-10% 90%',
                        end: '+=200px 100%',
                    },
                    yPercent: -200,
                    ease: "power1.inOut"
                })

                .from('.images-scroll-text-content-title .animated-title .animated-title__second-part', {
                    scrollTrigger: {
                        trigger: '.animated-title',
                        scroller: '#js-scroll',
                        scrub: 1,
                        start: '40% 90%',
                        end: '+=200px 100%',
                    },
                    yPercent: 200,
                    ease: "power1.inOut"
                })

                .from('.images-scroll-text-content-title .animated-title', {
                    scrollTrigger: {
                        trigger: '.animated-title',
                        scroller: '#js-scroll',
                        scrub: 1,
                        start: '80% 90%',
                        end: '+=400px 100%',
                    },
                    yPercent: -100,
                    ease: "power1.inOut"
                })
            }
        })

        ScrollTrigger.addEventListener("refresh", () => scrollCont.update());
        ScrollTrigger.refresh();
        
        $(".animsition-overlay").animsition({
            inClass: 'overlay-slide-in-top',
            outClass: 'overlay-slide-out-bottom',
            inDuration: 600,
            outDuration: 600,
            linkElement: '.animsition-link',
            loadingParentElement: 'body', //animsition wrapper element
            loading: false,
            loadingInner: '', // e.g '<img src="loading.svg" />'
            timeout: true,
            timeoutCountdown: 800, //2500
            onLoadEvent: false,
            unSupportCss: ["animation-duration", "-webkit-animation-duration", "-o-animation-duration", "-ms-animation-name"],
            browser: [ 'animation-duration', '-webkit-animation-duration'],
            // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
            // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
            overlay : true,
            overlayClass : 'animsition-overlay-slide',
            overlayParentElement : 'body',
            transition: function(url){
                window.location.href = url;
            },
        }).one('animsition.inStart',function(){
            $('body').removeClass('bg-init');
            $('.animsitions-loading').css('opacity','0');
       
        }).one('animsition.inEnd',function(){
            $('.animsitions-loading').css('opacity','1');
        }).one('animsition.outStart', function () {
           
        }).one('animsition.outEnd'), function() {
            
        }

        var titles = document.querySelectorAll('body:not(.no-header) .animated-title');
        titles.forEach(titleFunction);
       
        // var restLines = document.querySelectorAll('body:not(.no-header) .--first .images-text .images-text-content-text p');
        // restLines.forEach(restFunction);

        function restFunction(item, index) {
            item.setAttribute('data-scroll-speed', -2);
        }
        function titleFunction(item, index) {
            document.querySelector('body').classList.add('header-overlap');


            // var childSplit = new SplitText(item, {
            //     type: "lines",
            //     linesClass: "split-child"
            // });

            // var firstLine = childSplit.elements[0].childNodes[0];            
            // $(childSplit.elements[0]).children().not(':first-child').wrapAll('<div class="second-wrap" />');
            // var secondLine = childSplit.elements[0].childNodes[1];
            // var firstLineTargetId = '#' + item.parentNode.parentNode.querySelector('.images-text-content-text-scroll-target').id;

            // firstLine.setAttribute('data-scroll', '');
            // firstLine.setAttribute('data-scroll-id', '--first');
            // firstLine.setAttribute('data-scroll-speed', -3);
            // // firstLine.setAttribute('data-scroll-target', firstLineTargetId);
            
            // secondLine.setAttribute('data-scroll', '');
            // secondLine.setAttribute('data-scroll-id', '--rest');
            // secondLine.setAttribute('data-scroll-speed', 1.5);
            // secondLine.classList.add('second');
            // secondLine.querySelector('.split-child:first-child').classList.add('first-line');   
            
            // scrollCont.update();

            scrollCont.on('call', (args) => {
                if(typeof args.currentElements['firstPart'] === 'object') {
                   
                    var firstPart = args.currentElements['firstPart'],
                        secondPart = args.currentElements['secondPart'];


                    console.log({firstPart}, {secondPart});
                }
            });
        }

        if ( $('.is-inview .title-in')[0] ){
            var words = document.querySelectorAll('.is-inview .title-in h1 .word span');
            var wordsArray = [].slice.call(words);
            
            contentTl.to('.header .header-images picture', {
                duration: 4,
                opacity: "1",
                y: "0",
                ease: "Power4.easeOut",
                scale: 1,
            }, '1');
            contentTl.to(wordsArray, {
                duration: .8,
                y: "0",
                opacity: 1,
                ease: "Power4.easeOut",
                stagger: 0.13,
                delay: 0,
            }, "<+=0.2");
            contentTl.to('.header-content .paragraph', {
                duration: 3.5,
                opacity: 1,
                y: "0",
                ease: "Power4.easeOut",
            }, ">-0.3");
            contentTl.to('.header-content .buttons', {
                duration: 3.5,
                opacity: 1,
                y: "0",
                ease: "Power4.easeOut",
            }, "<");
        }

        scrollCont.stop();
        scrollCont.init();
        setTimeout(function(){
            scrollCont.update();
            scrollCont.start();
        }, 1000)
    });
        
} export {
    contentAnimationIn
} 