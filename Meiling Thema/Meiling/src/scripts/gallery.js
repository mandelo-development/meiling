
var $ = require("../../../config/node_modules/jquery");
window.$ = window.jQuery = require('../../../config/node_modules/jquery');
const fancybox = require('../../../config/node_modules/@fancyapps/fancybox');
var jQueryBridget = require('../../../config/node_modules/jquery-bridget');
var Masonry = require("../../../config/node_modules/masonry-layout");

async function gallery() {

    $( document ).ready(function() {
        $('.fancybox').fancybox();
    });

    jQueryBridget( 'masonry', Masonry, $ );

    $('.gallery').masonry({
        columnWidth: 1,
        percentPosition: true
    });
} export {
    gallery
}