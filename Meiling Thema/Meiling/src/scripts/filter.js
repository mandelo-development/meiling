import {scrollCont} from './locomotive_scroll';
var $ = require("../../../config/node_modules/jquery");
async function filter() {
    $(function() {
        $(document).on("change", ".category-selection-field input[type=checkbox]", function () {
            let e = $(".category-selection-field input[type=checkbox]:checked").map(function () {
                    return $(this).attr("data-slug")
                }).get().join(","),
                
                t = "/apis?categories=" + e + "&post_type=" + $(this).attr("data-post-type") + "&disable_edit_mode=1";

            console.log(e);
            window.history.pushState(null, null, "?categories=" + e);

            $.ajax({
                url: t,
                success: function (e) {
                    $("#post-card-container").html(e).hide().fadeIn(500);
                    $(window).trigger('load');
                }
            });
            
            setTimeout(function(){
                scrollCont.update();
            }, 400);
        });
    });

    $(function () {
        $(document).on("click", ".paginate-navigation-js a", function(e) {
            e.preventDefault();
            var t = $(this).attr("href").split("?")[1];
            console.log(t);
            t.includes("post_type") || (t += "&post_type=" + $(".category-selection-field input[type=checkbox]").attr("data-post-type"));
            var n = "/apis?" + t;
            "" != n && $.ajax({
                url: n,
                success: function(e) {
                    $("#post-card-container").html(e).hide().fadeIn(500), $("html,body").animate({
                        scrollTop: $("#post-card-container").offset().top
                    }, 500);
                    $(window).trigger('load');
                }
            })
        });
    });
} export {
    filter
}