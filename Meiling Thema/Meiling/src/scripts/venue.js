import gsap from '../../../config/node_modules/gsap';
var $ = require("../../../config/node_modules/jquery");

function venue() {
    const allVenues = gsap.utils.toArray(".collectie-item");
    const allAfspraken = gsap.utils.toArray(".afspraak-blok");
    const venueImageWrap = document.querySelector(".collectie-img-wrap");
    const venueImage = document.querySelector(".venues-img");
    var xpos = 0;
    var ypos = 0;

    function initVenues() {
        allVenues.forEach((link) => {
            link.addEventListener("mouseenter", venueHover);
            link.addEventListener("mouseleave", venueHover);
            link.addEventListener("mousemove", moveVenueImage);
        });

        allAfspraken.forEach((link) => {
            link.addEventListener("mouseenter", venueHover);
            link.addEventListener("mouseleave", venueHover);
            link.addEventListener("mousemove", moveVenueImage);
        });
    }

    function moveVenueImage(e) {
        xpos = e.clientX ;
        ypos = e.clientY;
        // console.log(xpos, ypos)
        const tl = gsap.timeline();
        tl.to(venueImageWrap, {
            x: xpos,
            y: ypos
        });
    }

    function venueHover(e) {
    if (e.type === "mouseenter") {
        setTimeout(function(){ 
            if (e.target.querySelector('img')){
                var targetImage = e.target.querySelector('img').src;
                venueImageWrap.classList.add('--img');
            } else if (e.target.dataset.cursor){
                
                var targetPrice = e.target.dataset.cursor;
                venueImageWrap.classList.add('--price');
                venueImageWrap.querySelector('span').textContent = targetPrice;
            }
        
            console.log(targetPrice)
            const t1 = gsap.timeline();
            t1.set(venueImage, {
                backgroundImage: `url(${targetImage})`
            }).to(venueImageWrap, {
                duration: 0.2,
                autoAlpha: 1,
                zIndex: 99,
                scale: 1,
            });
        }, 200)

    } else if (e.type === "mouseleave") {
        const tl = gsap.timeline();
        
        tl.to(venueImageWrap, {
            duration: 0.2,
            autoAlpha: 0,
            scale: 0.5,
        });
        setTimeout(function(){ 
            venueImageWrap.classList.remove('--price');
            venueImageWrap.classList.remove('--img');
            venueImageWrap.querySelector('span').textContent = '';
        }, 200)

    }
    }

    function init() {
        initVenues();
    }

    window.addEventListener("load", function () {
        init();
    });
} export {
    venue
}