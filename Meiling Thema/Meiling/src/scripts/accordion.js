import {scrollCont} from './locomotive_scroll';
var $ = require("../../../config/node_modules/jquery");

async function accordionClick() {
    $(document).ready(function() {
        var panelParent = $('.faq-wrapper');
        var panelItems = $(panelParent).find('.accordion');
        var mediaQuery = window.matchMedia('(min-width: 990px)');

        $(panelParent).each(function () {
            accordion(panelItems);
            if (mediaQuery.matches) {
                $(this).mouseenter(function(){
                    scrollCont.stop();
                }).mouseleave(function(){
                    scrollCont.start();
                    scrollCont.update();
                });
            }
        });
        function accordion(panelItems){
            let acc = panelItems;
            let i;

            for (i = 0; i < acc.length; i++) {
                acc[i].addEventListener("click", function() {
                    this.classList.toggle("active");
                    let panel = this.nextElementSibling;
                    if (panel.style.maxHeight){
                        panel.style.maxHeight = null;
                    } else {
                        panel.style.maxHeight = panel.scrollHeight + "px";
                    }
                });
                
            }
        }
    });
} export {
    accordionClick
}

