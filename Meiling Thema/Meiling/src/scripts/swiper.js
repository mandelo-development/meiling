// Import Swiper and modules
import {
	Swiper,
	Navigation,
	Pagination,
	Scrollbar,
	Controller,
	Lazy,
	Thumbs,
	Mousewheel,
	Autoplay,
	EffectFade,
	Parallax
} from '../../../config/node_modules/swiper/js/swiper.esm';
Swiper.use([Navigation, Pagination, Scrollbar, Controller, Lazy, Thumbs, Mousewheel, Autoplay, EffectFade, Parallax]);
import {scrollCont} from './locomotive_scroll';

async function swiperInstances() { 
	var $ = require('../../../config/node_modules/jquery');
	$(document).ready(function () {
		var dienstenSlider =  document.querySelectorAll('.diensten-slider-wrapper');
		if (typeof(dienstenSlider) != 'undefined' && dienstenSlider != null) {
			dienstenSlider.forEach((dienst) => {
				var dienstenSwiper = new Swiper(dienst, {
					spaceBetween: 30,
					loop: false,
					scrollbar: {
						el: '.swiper-scrollbar',
						draggable: true,
					},
					parallax: true,
					enabled: true,
					direction: 'horizontal',
					freeMode: false,
					mousewheel: {
						forceToAxis: true,
					},
					navigation: {
						nextEl: dienst.querySelector('.swiper-button-next'),
						prevEl: dienst.querySelector('.swiper-button-prev'),
					},
					breakpoints: {
						0: {
							slidesPerView: 1,
							spaceBetween: 20,
						},
						767: {
							slidesPerView: 1.5,
							spaceBetween: 30,
						},
						1024: {
							slidesPerView: 2,
							spaceBetween: 30,
						},
						1300: {
							slidesPerView: 2.4,
							spaceBetween: 60,
							slidesPerGroup: 3,
							freeMode: true,
						}, 1700: {
							slidesPerView: 2.8,
							spaceBetween: 60,
							slidesPerGroup: 2,
							freeMode: true,
						}
					}
				});

				dienstenSwiper.on('sliderMove', function(event){
					this.$el.addClass('grabbing');
				});
				dienstenSwiper.on('touchEnd', function(event){
					this.$el.removeClass('grabbing');
				});

			});
		}

		var imageSlider =  document.querySelectorAll('.image-slider-wrapper');
		if (typeof(imageSlider) != 'undefined' && imageSlider != null) {
			$('.image-slider-wrapper').each(function (index,value) {
				var sliderID = $(this).data('id-slider');
				var sliderDataId = '[data-id-slider="' + sliderID + '"]' ;
				var next_slider = 'swiper-button-next-id-' + sliderID;
				var prev_slider = 'swiper-button-prev-id-' + sliderID;
				
				var imageSwiper = new Swiper(sliderDataId, {
					slidesPerView: 1,
					spaceBetween: 30,
					loop: true,
					direction: 'horizontal',
					mousewheel: {
						forceToAxis: true,
					},
					navigation: {
						nextEl: ('.' + next_slider),
						prevEl: ('.' + prev_slider),
					},
					mousewheel: false,
					allowTouchMove: false,
					autoplay: {
						delay: 5000,
					},
				});
			});
		}


		var reviewSlider =  document.querySelectorAll('.review-slider-wrapper');
		if (typeof(reviewSlider) != 'undefined' && reviewSlider != null) {
			$('.review-slider-wrapper').each(function (index,value) {
				var sliderID = $(this).data('id-slider');
				var sliderDataId = '[data-id-slider="' + sliderID + '"]' ;
				var next_slider = 'swiper-button-next-id-' + sliderID;
				var prev_slider = 'swiper-button-prev-id-' + sliderID;
				var reviewSwiper = new Swiper(sliderDataId, {
					spaceBetween: 60,
					slidesPerView: 1,
					parallax: true,
					loop: false,
					direction: 'horizontal',
					
					navigation: {
						nextEl: ('.' + next_slider),
						prevEl: ('.' + prev_slider),
					},
					effect: 'fade'
					
				});
			});
		}

		var newsSlider =  document.querySelectorAll('.news-slider-wrapper');
		if (typeof(newsSlider) != 'undefined' && newsSlider != null) {
			newsSlider.forEach((news) => {
				console.log(news);
				var newsSwiper = new Swiper(news, {
					spaceBetween: 30,
					loop: false,
					parallax: true,
					enabled: true,
					scrollbar: {
						el: '.swiper-scrollbar',
						draggable: true,
					},
					direction: 'horizontal',
					mousewheel: {
						forceToAxis: true,
					},
					freeMode: false,
					navigation: {
						nextEl: news.querySelector('.swiper-button-next'),
						prevEl: news.querySelector('.swiper-button-prev'),
					},
					breakpoints: {
						0: {
							slidesPerView: 1,
							spaceBetween: 20,
						},
						767: {
							slidesPerView: 1.5,
							spaceBetween: 30,
						},
						1024: {
							slidesPerView: 2,
							spaceBetween: 30,
						},
						1300: {
							slidesPerView: 2.4,
							spaceBetween: 60,
							slidesPerGroup: 2,
							freeMode: true,
						}, 1700: {
							slidesPerView: 2.8,
							spaceBetween: 60,
							slidesPerGroup: 2,
							freeMode: true,
						}
					},
					
					
				});
				newsSwiper.on('sliderMove', function(event){
					this.$el.addClass('grabbing');
				});
				newsSwiper.on('touchEnd', function(event){
					this.$el.removeClass('grabbing');
				});
			});
		}
		

		var tabSlider =  document.querySelectorAll('.slider-container__tabs');
		if (typeof(tabSlider) != 'undefined' && tabSlider != null) {
			var tab_attr_title = Array.from(document.querySelectorAll('.slider-container__tabs .data-tab-slide')).map(function(x) {
				return x.getAttribute('data-tab-title');
			})
			var tabSwiper = new Swiper ('.slider-container__tabs', {
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
					renderBullet: function (index, className) {
						return '<div class="' + className + '">' + (tab_attr_title[index])+ 
						'</div>';
					},
				},
				loop: false,
				speed: 500,
				spaceBetween: 40,
				autoHeight: true,
			});

			tabSwiper.on('slideChange', function () {
				position_indicator($(".schedule").find(".swiper-pagination-bullet-active"));
			});
		}

		var menu = $(".swiper-pagination");
		if ($(".schedule__week .swiper-pagination-bullet-active")[0]){
			var indicator = $('<div class="indicator"></div>');
			menu.append(indicator);
			position_indicator(menu.find(".swiper-pagination-bullet-active"));  
		}
	
		function position_indicator(ele){
			var conleft = $('.schedule').offset().left;
			var left = ele.offset().left;
			var width = ele.outerWidth();
			var widthhalf =  width / 2;
			var centerLeft = conleft - left;
			indicator.stop().animate({
				left: Math.abs(centerLeft),
				width: width
			});
		}
		
	});
	
} export {
	swiperInstances
}