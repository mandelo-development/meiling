import {scrollCont} from './locomotive_scroll';
import {debounce, throttle, setActive} from './functions';
var $ = require("../../../config/node_modules/jquery");

async function highlight() {
    $(document).ready( function () {
        /* Highlight */ 
        var header__highlight = $('.header__highlight'),
        modal_container = $('.product-detail-container'),
        close_modal = $('.close-container'),
        product_detail = $('.product-detail-container .product-detail'),
        modal_single_highlight = $('.single__highlight');

        $(header__highlight).click( function (e) {
        var product_text = $(this).attr('data-product-text');
        $(modal_container).addClass('modal-open');
        $(product_detail).find('.product-text p').html(product_text);

        $(this).parent().parent().addClass('active_highlight');
        $(this).parent().siblings(modal_single_highlight).find(modal_container).removeClass('modal-open');

        e.preventDefault();
        });

        $(close_modal).click( function () {
        $('.active_highlight').removeClass('active_highlight');
        $(modal_container).removeClass('modal-open');
        });
    })
} export {
	highlight
}