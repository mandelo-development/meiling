import { debounce } from './functions';
import { scrollCont } from './locomotive_scroll';
var $ = require("../../../config/node_modules/jquery");

const tabs = () => {
    $(document).ready(function() {
        var mediaQuery = window.matchMedia('(min-width: 992px)');
        if(mediaQuery.matches) {
            var tabParent = $('.dynamic-tab');
            var tabItems = $(tabParent).find('.tab-item');
            var columnContent = $(tabParent).find('.column.content');
            $(tabParent).each(function () {
                setWidthHeight(tabItems);
            });
            function setWidthHeight(items) {
                $(items).each(function (index, element) {
                    var dataId = $(this).data('id');
                    var titleWidth = $(element).find('.tab-item__title').width();
                    var timesValue = titleWidth * dataId;
                    if(index === 1) {
                        $(items).css('width',"calc(100% - "+ timesValue +"px)")
                    }
                    $(element).css('transform', 'translateX(calc(100% - ' + timesValue + 'px))');
                }).promise().done( function(){
                    setPosition(items);
                });
                var tallestTab = Math.max.apply(Math, $(items).map(
                    function(){
                    return $(this).height();
                    }
                ));
                if(tallestTab > $(columnContent).outerHeight()) {
                    $(tabParent).css('height', tallestTab);
                }
                $(items).css('height', '100%');
                scrollCont.update();
            };
            function setPosition(items) {
                $(items).each(function (i, el) {
                    $(el).attr("data-position", JSON.stringify(getTranslate(el)));
                    if ($(el).hasClass('first')) {
                        setActive(el, true);
                    }
                    $(el).click(function() {
                        if ($(this).hasClass('active')) {
                            var nextAll = $(el).nextAll();
                            $(nextAll).each(function(index, nextElement){
                                setActive(nextElement, false);
                            });
                        } else {
                            var prevAll = $(el).prevAll();
                            setActive(el, true);
                            $(prevAll).each(function(index, prevElement){
                                setActive(prevElement, true);
                            });
                        }
                    });
                }).promise().done( function(){
                    $(tabParent).addClass('is-loaded');
                });
            };
            function getTranslate(item) {
                var transArr = [];
                if (!window.getComputedStyle) return;
                var style     = getComputedStyle(item),
                    transform = style.transform || style.webkitTransform || style.mozTransform || style.msTransform;
                var mat       = transform.match(/^matrix3d\((.+)\)$/);
                if (mat) return parseFloat(mat[1].split(', ')[13]);
                mat = transform.match(/^matrix\((.+)\)$/);
                mat ? transArr.push(parseFloat(mat[1].split(', ')[4])) : transArr.push(0);
                mat ? transArr.push(parseFloat(mat[1].split(', ')[5])) : transArr.push(0);
                return transArr;
            };
            function setActive(element, isActive) {
                var translateArray = getTranslate(element);
                var xValue = translateArray[0];
                var elTitleWidth = $(element).find('.tab-item__title').width();
                var titleTimes = $(element).data('title');
                var titleOffset = elTitleWidth * titleTimes;
                var activeWidth = xValue - xValue - titleOffset;
                var oldValue = $(element).data('position');
                if(isActive === true) {
                    $(element).addClass('active');
                    $(element).css('transform', 'translateX(' + activeWidth + 'px)');
                } else {
                    $(element).removeClass('active');
                    $(element).css('transform', 'translateX(' + oldValue[0] + 'px)');
                }
            };
            if ($(tabParent).length) {
                $(window).on('resize', debounce(function() {
                    setTimeout(() => {
                        setWidthHeight(tabItems);
                    }, 1000);
                }, 300));
            }
        } else {
            accordionClick();
            function accordionClick() {
                var acc = document.getElementsByClassName("tab-item__title");
                var i;
                var j;
                for (i = 0; i < acc.length; i++) {
                    acc[i].onclick = function() {
                        if (this.classList.contains("active")) {
                            this.nextElementSibling.style.maxHeight = null;
                            this.classList.remove("active");
                        } else {
                            for (j = 0; j < acc.length; j++) {
                                acc[j].nextElementSibling.style.maxHeight = null;
                                acc[j].classList.remove("active")
                            }
                            this.classList.add("active");
                            var panel = this.nextElementSibling;
                            if (panel.style.maxHeight) {
                                panel.style.maxHeight = null;
                            } else {
                                panel.style.maxHeight = panel.scrollHeight + "px";
                            }
                        }
                    }
                }
            }
        }
    });
}
export { 
    tabs
}