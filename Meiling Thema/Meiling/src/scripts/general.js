import {scrollCont} from './locomotive_scroll';
import {debounce, throttle, setActive} from './functions';
var $ = require("../../../config/node_modules/jquery");
import gsap from '../../../config/node_modules/gsap';
var $ = require("../../../config/node_modules/jquery");
gsap.config({nullTargetWarn: false});
var contentTl = gsap.timeline();

async function menu() {
    $(document).ready( function () {
        $('.bars').click(throttle(function() {
            var clicks = $(this).data('clicks');
            var navigation = $('.navigation');
        
            if (!clicks) {
                $(navigation).addClass('mb-menu-open');
                $(this).addClass('bars-active');
            } else {
                $(navigation).removeClass('mb-menu-open');
                $(navigation).removeClass('mb-menu-pushed');
                $(this).removeClass('bars-active');
            }
            $(this).data("clicks", !clicks);
        }, 1000));

        $('.dropdown-menu-dropdown-icon-back').click(throttle(function() {
            var clicks = $(this).data('clicks');
            var navigation = $('.navigation');
        
            if (!clicks) {
                $(navigation).removeClass('mb-menu-pushed');
            } else {
                $(navigation).addClass('mb-menu-pushed');
            }
            $(this).data("clicks", !clicks);
        }, 1000));

        $('.dropdown-menu-dropdown-icon').click(throttle(function() {
            var dropID = $(this).next()[0].getAttribute('data-dropdown-id');
            var clicks = $(this).data('clicks');
            var navigation = $('.navigation');
            if (!clicks) {
                $(navigation).addClass('mb-menu-pushed');
            } else {
                $(navigation).removeClass('mb-menu-pushed');
            }

            $('.navbar-subs-menu [data-dropdown-id]').each(function() {
                if ($(this)[0].getAttribute('data-dropdown-id') == dropID){
                    $(this).show();
                } else {
                    $(this).hide();    
                }
            });
            $(this).data("clicks", !clicks);
        }, 1000));
       
    });
}

async function general() {
    $(document).ready( function () {
        [].forEach.call(
            document.querySelectorAll('.form-field__label, .form-field input, .form-field__textarea'),
            el => {
                el.onblur = () => {
                    setActive(el, false);
                };
                el.onfocus = () => {
                    setActive(el, true);
                };
        });

        console.log(document.querySelector('.o-scroll').classList.contains('--disable'))
        
        let configMode;
        if(parent.document.getElementById('config-bar') || document.querySelector('.o-scroll').classList.contains('--disable') == true) {
            $('body').addClass('config-mode');
            configMode = true;
            scrollCont.destroy();

        } else {
            configMode = false;
        }

        $('a[href*=\\#]').click(function(event) {
            if(parent.document.getElementById('config-bar')) {
            } else {
                event.preventDefault();
                var hash = event.currentTarget.hash;
                var hashSplit = hash.split("#");
                var linkPoint = document.getElementById(hashSplit[1]);
                scrollCont.scrollTo(linkPoint, -50, 1000)
                scrollCont.update();
            }
        });
    
        var didScroll;
        var lastScrollTop = 0;
        var delta = 50;
        var navbar = $('.navigation');
        var header = $('header');
        var navbarHeight = $(navbar).outerHeight();
        var headerHeight = $(header).outerHeight();
        var $height = Math.abs($('#js-scroll').offset().top);
    
        
        if (configMode == false){
            var media = window.matchMedia("(min-width: 1024px)")
            if (media.matches) {
               
                scrollCont.on('scroll', function (event) {
                    didScroll = true;
                    changeBackgroundColor();

                   
                });

                scrollCont.on('call', (obj) => {
                    console.log(obj)
                });

                
            } else {
                scrollCont.destroy();
                $(window).scroll(function() {
                    changeBackgroundColor('smaller');
                    
                });
            }
        }
    
       

        setInterval(function() {
            if (didScroll) {
                hasScrolled();
                
                didScroll = false;
            }

           
        }, 250);
    
        function hasScrolled() {
            var offsetScroll = $('#js-scroll').offset().top;
            // var bodyHeight = $('#js-scroll').height();
            
            // var footerHeight = $('.footer-footer').outerHeight() + $('.footer-copyright').outerHeight();
            // console.log(offsetScroll)
            var st = Math.abs(offsetScroll);
            // console.log({bodyHeight}, {footerHeight})

            var paragraphs = document.querySelectorAll('.footer-footer-content.is-inview .paragraph'), i;
            console.log(paragraphs)
            for (i = 0; i < paragraphs.length; ++i) {
                var singleWords =  paragraphs[i].getElementsByTagName('p'); 
                console.log(singleWords)
                if (singleWords.length != 0){
                    var wordsArray = [].slice.call(singleWords);
                    contentTl.to(wordsArray, {
                        x: "0",
                        duration: .8,
                        stagger: .05,
                        opacity: "0.75",
                        ease: "Power4.easeOut",
                        delay: 1
                    }, '0');
                }
            }

            if (st > 50){
                $(navbar).addClass('nav-fixed');
            } else {
                $(navbar).removeClass('nav-fixed');
            }

           

            

           
               
            

            if(Math.abs(lastScrollTop - st) <= delta)
                return;
            
            if (navbarHeight > st) {
                $(navbar).addClass('nav-top');
            } else {
                $(navbar).removeClass('nav-top');
            }

            if (headerHeight > st) {
                $(navbar).addClass('nav-header-iviw');
            } else {
                $(navbar).removeClass('nav-header-iviw');
            }
            
            if (st > lastScrollTop && st > navbarHeight){
                $(navbar).removeClass('nav-up').addClass('nav-down');
            } else {
                if(st + $(window).height() < $(document).height()) {
                    $(navbar).removeClass('nav-down').addClass('nav-up');
                }
            }
            lastScrollTop = st;
        }
    
        function changeBackgroundColor(wid){
            var $section = $('.change-bg-color'),
                $window = $(window),
                $body = $('body'),
                $main = $('.main-content-wrapper'),
                $nav = document.getElementById("navigation"),
                $titles = $('.big-title-element'),
                $images = $('.images-text-side .parralax-images'),
                $line = $('.footer-footer .line');
                
            if (wid == 'smaller') {
                var scroll = $window.scrollTop() + ($window.height() / 1.1);
            } else {
                var $height = Math.abs($('#js-scroll').offset().top);
                var scroll = $height + ($window.height() / 1.1);
            }
            

            if($('.images-text-side .parralax-images').length) {
                
                if (wid == 'smaller'){
                    $images.each(function () {
                        var $this = $(this);
                        var positionTop = $this.offset().top;
                        var height = $this.height();
                        var scroll = $window.scrollTop() + ($window.height() - height);
                        console.log({positionTop}, {scroll})
                        if (positionTop <= scroll) {
                            $this.addClass('is-inview');
                        }
                    });
                }
            }

            if($('.change-bg-color').length) {
                $section.each(function () {
                    var $this = $(this);
                    var positionTop = $this.position().top;
                    var height = $this.height();
    
                    $this.css('background', 'transparent');
                    
                    if (positionTop <= scroll && positionTop + height > scroll) {
                        $main.css('background', $(this).data('color'));
                        $nav.setAttribute('data-invert', $(this).data('invert'));
                        
                        $body.removeClass(function (index, css) {
                            return (css.match (/(^|\s)color-\S+/g) || []).join(' ');
                        });
    
                        $body.addClass('color-' + $(this).data('color'));
                    }
                });
            }


            if($('.big-title-element').length) {
                $titles.each(function () {
                    var $this = $(this);
                    var positionTop = $this.position().top;
                    var height = $this.height();

                    if (positionTop <= scroll && positionTop + height > scroll) {
                        var wordsBig = document.querySelectorAll('.big-title-element .word span');
                        var wordsArrayBig = [].slice.call(wordsBig);
                        
                        contentTl.to(wordsArrayBig, {
                            duration: .8,
                            y: "0",
                            opacity: "1",
                            ease: "Power4.easeOut",
                            stagger: 0.2,
                        }, '+=0');

                        contentTl.to('.footer-cta .button-wrapper', {
                            duration: .8,
                            y: "0",
                            opacity: "1",
                            ease: "Power4.easeOut",
                            stagger: 0.13,
                            delay: 0,
                        }, '>');
                    }
                });
            }

            if($('.footer').length) {
                $line.each(function () {
                    var $this = $(this);
                    var positionTop = $this.position().top;
                    var height = $this.height();

                    if (positionTop <= scroll && positionTop + height > scroll) {
                        alert('line')
                        contentTl.to($this, {
                            duration: .8,
                            y: -100,
                            opacity: "1",
                            ease: "Power4.easeOut",
                            stagger: 0.13,
                            delay: 0,
                        }, '>');
                    }
                });
            }
        }
        
    
        var intersectionOptions = {
            root: null,  // use the scrollContainer
            rootMargin: '0px 0px 20% 0px',
            threshold: 0.5,
        };
    
        function intersectionCallbackConts(entries, observer) {
            $(entries).each(function( i, entry ) {
                let elem = entry.target;
                if (entry.intersectionRatio > intersectionOptions.threshold) {
                    var elements = elem.getElementsByClassName("anim-item");
                    $(elements).each(function( i, element ) {
                        var delay = parseFloat($(element).data('delay'));
                        // console.log(delay)
                        setTimeout(function(){  $(element).addClass('animated'); }, delay);
                    });
                } else {
                    // console.log("Not fully visible!");
                }
            });
        }
    
        var observer = new IntersectionObserver(intersectionCallbackConts, intersectionOptions);
    
        var animItems = document.querySelectorAll('.anim-elem');
    
        $(animItems).each(function( i, element ) {
            observer.observe(element);
        });
    
        function accordionClick() {
            let acc = document.getElementsByClassName("accordion");
            let i;
        
            for (i = 0; i < acc.length; i++) {
                acc[i].addEventListener("click", function() {
                    this.classList.toggle("active");
                    let panel = this.nextElementSibling;
                    if (panel.style.maxHeight){
                        panel.style.maxHeight = null;
                    } else {
                        panel.style.maxHeight = panel.scrollHeight + "px";
                    }
                });
            }
        } 
    });
}

export {
    general, menu
}