{%- comment %}Defined breakpoints{% endcomment -%}
{% assign breakpoints = imageSizes | split: '|' %}
{% capture breakpointInputs %}[
	{%- for breakpoint in breakpoints -%}
		{% assign breakpointArray = breakpoint | split: '-' %}
		{
			"name": "{{breakpointArray[0]}}",
			"width": "{{breakpointArray[1]}}",
			"height": "{{breakpointArray[2]}}"
		}{% unless forloop.last %},{% endunless %}
	{%- endfor -%}
]{% endcapture %}
{% assign breakpointInputs  = breakpointInputs | from_json  %}
{%- comment %}Input for boxed array{% endcomment -%}
{%- assign arrayBoxedInput = '1200-1200-xl,992-992-lg,768-768-md,576-768-sm' | split: ',' -%}
{% capture arrayBoxedViewports %}[
	{%- for viewport in arrayBoxedInput -%}
		{% assign viewportArray = viewport | split: '-' %}
		{
			"name": "{{viewportArray[2]}}",
			"container": {{viewportArray[1]}},
			"window": {{viewportArray[0]}}
		}{% unless forloop.last %},{% endunless %}
	{%- endfor -%}
	{% if site.extra_viewportssd.size >= 1 %},
	{%- for viewport in site.extra_viewports -%}
	{
		"name": "{{viewport.name_viewport}}",
		"container": {{viewport.width_viewport}},
		"window": {{viewport.width_viewport}}
	}{% unless forloop.last %},{% endunless %}
	{%- endfor -%}
	{% endif %}
]{% endcapture %}
{%- comment %}Input for unboxed array{% endcomment -%}
{%- assign arrayUnboxedInput = '3100-3800-4k,2560-3100-xxxxxxl,2200-2560-xxxxxl,1920-2200-xxxxl,1600-1920-xxxl,1400-1600-xxl,1200-1400-xl,992-1200-lg,768-992-md,576-768-sm' | split: ','-%}{% comment %}Append with extra breakpoints (only for boxed content){% endcomment %}
{% capture arrayUnboxedViewports %}[
	{%- for viewport in arrayUnboxedInput -%}
		{% assign viewportArray = viewport | split: '-' %}
		{
			"name": "{{viewportArray[2]}}",
			"container": {{viewportArray[1]}},
			"window": {{viewportArray[0]}}
		}{% unless forloop.last %},{% endunless %}
	{%- endfor -%}
]{% endcapture %}
{% comment %}Boxed or not{% endcomment %}
{%- if imageBoxed == true -%}
	{% assign arrayViewports = arrayBoxedViewports | from_json | sort: 'min' %}
{%- else -%}
	{% assign arrayViewports = arrayUnboxedViewports | from_json | sort: 'min'  %}
{%- endif -%}
{%- comment %}Combine arrays and create specifications for each viewport{% endcomment -%}
{% capture imageArray %}[
	{% assign firstBreakpoint = breakpointInputs | where: 'name', '$' | first %}
	{% assign thisWidth = firstBreakpoint.width %}
	{% assign thisHeight = firstBreakpoint.height %}
	{
		"name": "$",
		"min": 0,
		"max": {{arrayViewports[1].window | minus: 1}},
		"width": {{thisWidth | remove: '%' | remove: 'px' | times: 1 }},
		"width_type":"
		{%- if thisWidth contains 'px' -%}
			absolute
		{%- elsif thisWidth contains '%' -%}
			relative
		{%- elsif thisWidth == "0" -%}
			ignore
		{%- else -%}
			columns
		{%- endif -%}",
		"height": {{thisHeight | remove: '%' | remove: 'px' | times: 1 }},
		"height_type":"
		{%- if thisHeight contains 'px' -%}
			absolute
		{%- elsif thisHeight contains '%' -%}
			relative
		{%- else -%}
			ignore
		{%- endif -%}",
		"window": {{arrayViewports[1].window | minus: 1}},
		"container": {{arrayViewports[1].container}}
	},
	{%- for viewport in arrayViewports -%}
		{%- unless viewport.name == "$" -%}
				{% assign nextIndex = forloop.index0 | plus: 1 %}
				{% assign thisBreakpoint = breakpointInputs | where: 'name', viewport.name | first %}
				{%- if thisBreakpoint.width and thisBreakpoint.width != "" -%}
					{% assign thisWidth = thisBreakpoint.width %}
				{%- endif -%}
				{%- if thisBreakpoint.height and thisBreakpoint.height != "" -%}
					{% assign thisHeight = thisBreakpoint.height %}
				{%- endif -%}
				{
					"name": "{{viewport.name}}",
					"min": {{viewport.window}},
					{%- if arrayViewports[nextIndex] -%}
						"max": {{arrayViewports[nextIndex].window | minus: 1}},
					{%- endif -%}
					"width": {{thisWidth | remove: '%' | remove: 'px' | times: 1 }},
					"width_type":"
					{%- if thisWidth contains 'px' -%}
						absolute
					{%- elsif thisWidth contains '%' -%}
						relative
					{%- elsif thisWidth == "0" -%}
						ignore
					{%- else -%}
						columns
					{%- endif -%}",
					"height": {{thisHeight | remove: '%' | remove: 'px' | times: 1 }},
					"height_type":"
					{%- if thisHeight contains 'px' -%}
						absolute
					{%- elsif thisHeight contains '%' -%}
						relative
					{%- else -%}
						ignore
					{%- endif -%}",
					"window": {{viewport.window}},
					"container": {{viewport.container}}
				}{% unless forloop.last %},{% endunless %}
			{%- endunless -%}
	{%- endfor -%}
]{% endcapture %}
{% assign imageArray = imageArray | from_json | sort: 'min'  %}
